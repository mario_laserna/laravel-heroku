<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        //Crea un usuario por cada carga de esta pagina
        User::create([
            'name' => 'nombre'.rand(1,999),
            'email' => 'email'.rand(1,999),
            'password' => bcrypt(str_random(10)),
        ]);

        $users = User::all();
        return view('test')->with(['users'=>$users]);
    }
}
